package com.mitocode.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.model.Curso;
import com.mitocode.service.ICursoService;

@Named
@ViewScoped
public class CursoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Curso curso;

	@Inject
	private ICursoService service;
	private List<Curso> lista;

	@PostConstruct
	public void init() {
		lista = new ArrayList<>();
		listar();
	}

	public void listar() {
		lista = service.listar();

	}

	public void seleccionar(Curso c) {
		curso = c;
	}

	// Getters & Setters
	public List<Curso> getLista() {
		return lista;
	}

	public void setLista(List<Curso> lista) {
		this.lista = lista;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
