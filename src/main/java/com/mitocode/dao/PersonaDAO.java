package com.mitocode.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import com.mitocode.model.Curso;
import com.mitocode.model.Persona;

@Named
public class PersonaDAO implements IDAO, Serializable{
	
	@Override
	public List<Persona> listar() {
		List<Persona> lista = new ArrayList<>();
		for (int i = 0; i < 50; i++) {
			Persona per = new Persona();
			per.setIdPersona(i);
			per.setNombres("Jaime");
			per.setApellidos("Medina");
			per.setEdad(27);
			lista.add(per);
		}
		
		return lista;
	}


}
