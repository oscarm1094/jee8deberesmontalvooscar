package com.mitocode.dao;

import java.util.ArrayList;
import java.util.List;

import com.mitocode.model.Persona;

public class PersonaDAO2 {//implements IDAO{

	//@Override
	public List<Persona> listar() {
		List<Persona> lista = new ArrayList<>();
		for (int i = 0; i < 200; i++) {
			Persona per = new Persona();
			per.setIdPersona(i);
			per.setNombres("Jaime");
			per.setApellidos("Medina");
			per.setEdad(27);
			lista.add(per);
		}
		
		return lista;
	}
}
