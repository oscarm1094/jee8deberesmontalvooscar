package com.mitocode.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import com.mitocode.model.Curso;
@Named
public class CursoDAO implements IDAOCurso, Serializable{

	private static final long serialVersionUID = 1L;

	@Override
	public List<Curso> listar() {
		List<Curso> lista = new ArrayList<>();
		
		for (int i = 0; i < 50; i++) {
			StringBuilder sb = new StringBuilder();
			Curso curso = new Curso();
			curso.setIdCurso(i);
			curso.setNombre(sb.append("curso ").append(String.valueOf(i)).toString());
			curso.setHoras(i*2);
			lista.add(curso);
		}
		
		return lista;
	}

}
