package com.mitocode.dao;

import java.util.List;

import com.mitocode.model.Curso;

public interface IDAOCurso {
	List<Curso> listar();
}
