package com.mitocode.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.dao.IDAOCurso;
import com.mitocode.model.Curso;
@Named
public class CursoServiceImpl implements ICursoService, Serializable{

	@Inject
	private IDAOCurso curso;
	
	@Override
	public List<Curso> listar() {
		return curso.listar();
	}
	
}
