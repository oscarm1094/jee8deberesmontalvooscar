package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Curso;

public interface ICursoService {
	List<Curso> listar();
}
